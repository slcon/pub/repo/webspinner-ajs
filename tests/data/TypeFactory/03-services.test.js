﻿/*******************************************************************************
* Written by Steve Thames                                              1/22/2021
* ------------------------------------------------------------------------------
* TypeFactory service tests.
*******************************************************************************/
require('./_common.js');

describe('Services',function() {
  var Data, TypeFactory;

  beforeEach(() => {
    angular.mock.module('wsajs.data', (TypeFactoryProvider) => {
      TypeFactoryProvider.addType({ name: 'Data.Class1', $type: 'Class1' });
      TypeFactoryProvider.addType({ name: 'Data.Class2', $type: 'Class2', inherits: 'Data.Class1' });
      TypeFactoryProvider.addType({ name: 'Data.Class3', $type: 'Class3', inherits: 'Data.Class2' });
      TypeFactoryProvider.addType({ name: 'Data.Class4' });
    });

    angular.mock.inject((_Data_, _TypeFactory_) => {
      Data = _Data_;
      TypeFactory = _TypeFactory_;
    });
  });

  test('mapObject', () => {
    var a = TypeFactory.mapObject(Data.Class1)({ name:"Jane", $type:"Class3"});
    expect(a instanceof Data.Class3).toBe(true);

    var b = TypeFactory.mapObject(Data.Class4)({ name:"Jane" });
    expect(b instanceof Data.Class4).toBe(true);
  });

  test('mapList', () => {
    var a = TypeFactory.mapList(Data.Class1)([
      { name:"Jane", $type:"Class2" },
      { name:"Jane", $type:"Class3" },
      { name:"Jane" }
      ]);
    expect(a[0] instanceof Data.Class2).toBe(true);
    expect(a[1] instanceof Data.Class3).toBe(true);
    expect(a[2] instanceof Data.Class1).toBe(true);
  });
});
