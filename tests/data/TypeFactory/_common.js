﻿/*******************************************************************************
* Common require for TypeFactory unit tests.
*******************************************************************************/
require('../../_common.js');
require('../../../src/data/data.js');
require('../../../src/data/TypeFactory.js');
require('../../../src/polyfill/polyfill.js');
require('../../../src/polyfill/Object.js');
