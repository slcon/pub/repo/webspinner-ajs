﻿/*******************************************************************************
* TypeFactory configuration happens in the AngularJS config phase, where types
* are defined, and the run phase, where they are created. Exceptions are thrown
* from the TypeFactoryProvider `addType` and `$get` methods when they are called 
* as part of these phases. This module is required by any tests that want to
* manage the exceptions rather than let `jest` handle them.
* 
* The `addType` and `$get` methods are mocked and `angular.mock.inject()` is
* called in a try block so an exception thrown by these methods will not kill 
* `jest`. `expect().toHaveThrown()` is added by this module and is used to check 
* for exceptions thrown.
* 
* ## API
* 
* ### addType(type)
* 
*   * `{object}`**`type`**  
*     `TypeFactory` class definition.
* 
* Adds a class definition that will be passed to `TypeFactoryProvider.addType()` 
* in the config phase.
* 
* ### expect(method)
* 
*   * `{string}`**`method`**  
*     `TypeFactoryProvider` method name.
*     
* Returns a call to Jest `expect(TypeFactoryProvider[method])` for use with 
* [Expect Matchers](https://jestjs.io/docs/expect).
* 
* ### inject([capture])
* 
*   * `{boolean}`**`capture`**  
*     `true` uses a `try` block to swallow any exception thrown.  
* 
* Calls `angular.mock.inject()` to fire the config and run phases.
* 
* ### success
* 
* `true` unless and exception was thrown by `addType` or `$run` following a call
* to `inject()`.
* 
* ## Example
* 
*     var mock = require('./_mock-provider.js');
*     
*     mock.addType({
*       name:     'MyClass',
*       defaults: {
*         name: null,
*         age:  0
*       }
*       constructor: {}
*     });
*     
*     mock.inject();
*     mock.expect('addType').toHaveThrown();
*******************************************************************************/
require('./_common.js');

/*******************************************************************************
* expect().toHaveThrown */
/**
* Checks all the results of calls to a mocked method to verify an exception was 
* thrown. `message` may be empty (any exception), a Regular Expression, or a
* text fragment to search for in the exception text.
*
* @param  {MockFn}        mockFn  - Mock function.
* @param  {string|RegExp} message - Exception message fragment or regular expression.
*                                   `null`=any exception.
*******************************************************************************/
expect.extend({ toHaveThrown(mockFn, message)
  {
  var ex   = mockFn.mock.results.filter((r) => r.type === 'throw').pop();
  var pass = { pass: true,  message: () => `${mockFn.getMockName()} threw "${ex.value}"` };
  var fail = { pass: false, message: () => `expected ${mockFn.getMockName()} to throw ${!!message ? '"'+message+'"' : 'an exception'}.` };

  if (ex)
    if (!message)
      return pass;
    else if (message instanceof RegExp && message.test(ex.value))
      return pass;
    else if (ex.value.indexOf(message) >= 0)
      return pass;
  return fail;
  }});

/*******************************************************************************
* configProvider */
/**
* Passed to `angular.mock.module` to mock the `addType` and `$get`methods of
* TypeFactoryProvider in the config phase. Assigns `provider` to the injected 
* TypeFactoryProvider and calls `addType` for all the definitions in `types`. 
* Clears `types` allowing pattern reuse without `beforeEach` or `afterEach`.
*******************************************************************************/
var provider = null;
var types    = [];

function configProvider(TypeFactoryProvider, $injector)
  {
  /*---------------------------------------*/
  /* Assign `provider` and mock `addType`. */
  /*---------------------------------------*/
  provider         = TypeFactoryProvider;
  provider.addType = jest.fn().mockName('addType').mockImplementation(provider.addType);

  /*------------------------------------------------------------------------------------*/
  /* Service constructor ($get) can't simply be mocked but must be injected             */
  /* using the service injector which is not available until the service is             */
  /* instantiated in the run phase. The old "chicken and egg" problem.                  */
  /*                                                                                    */
  /* Replace `$get` with a function that will be injected with the service              */
  /* injector ($injector), mock the constructor to use the service injector             */
  /* to invoke the actual `$get`, and execute the mock.                                 */
  /*                                                                                    */
  /* There may be a better way to do this but I couldn't think of one. Some             */
  /* help can be found here:                                                            */
  /* https://medium.com/@a_eife/testing-config-and-run-blocks-in-angularjs-1809bd52977e */
  /*------------------------------------------------------------------------------------*/
  provider.$get = (function($get)
    {
    return function($injector)
      {
      provider.$get = jest.fn().mockName('$get').mockImplementation(() => $injector.invoke($get, provider));
      provider.$get();
      };
    })(provider.$get);

  /*---------------------------------------*/
  /* Clear the `types` array first because */
  /* `addType` may throw an exception.     */
  /*---------------------------------------*/
  var tlist = types;
  types = [];
  tlist.forEach((type) => provider.addType(type));
  }

/*******************************************************************************
* API
*******************************************************************************/
module.exports = 
  {
  /*------------------------------------------------------*/
  /* Add a type (class) definition to be added using      */
  /* `TypeFactoryProvider.addType()` in the config phase. */
  /*------------------------------------------------------*/
  addType: (type)   => types.push(type),

  /*----------------------------------*/
  /* Return `expect(provider[method]) */
  /* for use with `expect` functions. */
  /*----------------------------------*/
  expect:  (method) => expect(provider[method]),

  /*----------------------------------------*/
  /* `true` when no errors were encountered */
  /* in the config and run phases.          */
  /*----------------------------------------*/
  get success() { return (this.expect('addType').not.toHaveThrown() && this.expect('$get').not.toHaveThrown()); },

  /*------------------------------------------*/
  /* Fire the config/run phases. If `capture` */
  /* is true, capture thrown exceptions.      */
  /*------------------------------------------*/
  inject:  (capture)  => 
    {
    angular.mock.module('wsajs.data', configProvider);

    if (capture)
      try { angular.mock.inject(); } catch(e) {}
    else
      angular.mock.inject();
    }
  };

















