/*******************************************************************************
* Written by Steve Thames                                              1/22/2021
/**
* @ngdoc    overview
* @name     wsajs-net.data
* @description
* Data management.
*******************************************************************************/
angular.module('wsajs.data', ['wsajs.polyfill']);
