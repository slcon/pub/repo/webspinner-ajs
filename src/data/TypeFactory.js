﻿/*******************************************************************************
* Written by Steve Thames                                              1/22/2021
/**
* @ngdoc       service
* @name        TypeFactoryProvider
* @module      wsajs.data
* @description 
* Provides configuration of type "classes" that correspond to server C# classes.
*******************************************************************************/
(function() { "use strict";
angular.module('wsajs.data').provider('TypeFactory', function($injector) 
  {
  this.addType = $injector.invoke(addType, this);
  this.$get    = $injector.invoke($get, this);

  /**
  * @ngdoc property
  * @name  TypeFactoryProvider#spaces
  * @description
  * List of `TypeSpace` objects created as factories. `addType` creates this as a list of 
  * factory names and `service` maps them to the factory objects for use in `service.makeInstance`.
  */
  this.spaces  = [];

  /**
  * @ngdoc property
  * @name  TypeFactoryProvider#types
  * @description
  * Object of class type specifications keyed by `name` property. Used in `addType`
  * and `service` to first define and then create types.
  */
  this.types   = {};
  })

/*---------------------------------------------------------*/
/* Instantiate the service to complete the Typespace Tree. */
/*---------------------------------------------------------*/
.run(function(TypeFactory) {});

/*******************************************************************************
* TypeFactoryProvider.addType */
/**
* @ngdoc       function
* @name        addType
* @methodOf    csm.books.services.TypeFactoryProvider
* @param       {object} spec  Type specification.
* @description 
* Adds type specification to `types` and creates factories for services defined
* in `spec.name`. 
*******************************************************************************/
/* @ngInject */ function addType($injector, $provide)
  {
  return function(spec)
    {
    /*----------------------*/
    /* Validate input spec. */
    /*----------------------*/
    if (!angular.isObject(spec) || angular.isArray(spec))
      throw providerError(null, "input 'spec' must be an object.");

    if (!spec.name)
      throw providerError("'spec.name' is required.");

    if (spec.inherits === spec.name)
      throw providerError(spec.name, "cannot inherit itself.");

    if (spec.defaults && !(angular.isObject(spec.defaults) && !angular.isArray(spec.defaults)) && !angular.isFunction(spec.defaults))
      throw providerError(spec.name, "'defaults' must be an object or injectable function returning an object.");
  
    if (spec.prototype && !(angular.isObject(spec.prototype) && !angular.isArray(spec.prototype)) && !angular.isFunction(spec.prototype))
      throw providerError(spec.name, "'prototype' must be an object or injectable function returning an object.");
  
    if (spec.constructor && !angular.isFunction(spec.constructor))
      throw providerError(spec.name, "'constructor' must be an injectable function returning a function.");

    if (spec.compile && !angular.isFunction(spec.compile))
      throw providerError(spec.name, "'compile' must be an injectable function returning an object.");

    /*-----------------------------------------------------------*/
    /* Create type service class will be created in, if needed.  */
    /*                                                           */
    /* Note: $injector.has() will find AngularJS services here   */
    /* but will not find them in `$get.createType`. I don't know */
    /* why but this means if we didn't create it, it likely      */
    /* won't be found there or will not be a TypeSpace object.   */
    /*-----------------------------------------------------------*/
    var ns = new NameSpecs(spec.name);

    if (!$injector.has(ns.serviceName) && !ns.serviceName.match(/^TypeFactory/) && !ns.serviceName.match(/Provider$/))
      {
      $provide.factory(ns.serviceName, function() { return new TypeSpace(); });
      this.spaces.push(ns.serviceName);
      }

    /*--------------------------------------*/
    /* Add spec to list of types to create. */
    /*--------------------------------------*/
    this.types[spec.name] = spec;
    };
  }

/*******************************************************************************
* TypeFactoryProvider.$get */
/**
* @ngdoc       service
* @name        TypeFactory
* @module      csm.books.services
* @description
* Called in `.run` block to finish creation of type constructors. Creates types
* for all definitions in `types` and maps service names in `spaces` to service 
* factories for use in `makeInstance()`.  
*******************************************************************************/
/* @ngInject */ function $get()
  {
  return function($injector)
    {
    this.createType   = $injector.invoke(createType,   this);
    this.makeFactory  = $injector.invoke(makeFactory,  this);
    this.makeInstance = $injector.invoke(makeInstance, this);
    this.service      = $injector.invoke(service,      this);

    /*--------------------------------*/
    /* Create all the type factories. */
    /*--------------------------------*/
    for (var name in this.types)
      this.createType(name);
    delete this.types;

    /*--------------------------------------------------*/
    /* Convert list of typespace factory names to       */
    /* factory list for use in 'TypeSpace.newInstance'. */
    /*--------------------------------------------------*/
    this.spaces = this.spaces.map(function(s) { return $injector.get(s); });

    return this.service;
    };
  }

/*******************************************************************************
* TypeFactory Service */
/**
* Returns the TypeFactory service object. 
*******************************************************************************/
/* @ngInject */ function service($injector)
  {
  return 0,
    {
    /**
    * @ngdoc      method
    * @name       TypeFactory#mapObject
    * @param      {object}   cls Class constructor of object to create.
    * @return     {function}     Function taking input of object.
    * @description
    * Returns a function that takes `{object}` and returns a call to the 
    * `cls.newInstance()` method to convert an anonymous object to a 
    * **`class`** or subclass object based on `cls.$type`. If `cls.newInstance`
    * is not found, returns a call to `new cls(obj)`.
    */
    mapObject: (cls) => (obj) => (typeof cls.newInstance == 'function' ? cls.newInstance(obj) : new cls(obj)),
  
    /**
    * @ngdoc      method
    * @name       TypeFactory#mapList
    * @param      {object}   cls Class constructor of objects to create.
    * @return     {function}     Function taking input of object list.
    * @description
    * Returns a function that takes an array of `{object}`, calling 
    * mapObject(cls) for each, and returning a list of **`class`** or 
    * subclass objects.
    */
    mapList: function(cls) 
      { 
      var mo = this.mapObject(cls);
      return (list) => list.map(obj => mo(obj)); 
      }
    };
  }

/*******************************************************************************
* TypeFactoryProvider.createType */
/**
* Called when service is instantiated for each type given to provider. Creates
* `TypeSpace` for each segment of the namespace, if any, and creates object 
* factory (constructor) in the last namespace segment node or in the service
* factory `TypeSpace` if there is no namespace. 
*
* @param {string} name  Client-side type name.
*******************************************************************************/
/* @ngInject */ function createType($injector)
  {
  return function(name)
    {
    var spec = this.types[name];

    if (!spec)
      throw providerError(null, `class '${name}' not found.`);

    if (!spec.typeSpace)
      {
      /*------------------------------------------*/
      /* Create the parent type first, if needed. */
      /*------------------------------------------*/
      if (!!spec.inherits)
        this.createType(spec.inherits);

      /*----------------------------------------------------------------------*/
      /* Assign the service factory as the typespace for this object factory. */
      /* If type service does not exist, we didn't create it. See `addType`.  */
      /*----------------------------------------------------------------------*/
      var ns = new NameSpecs(spec.name);

      if ($injector.has(ns.serviceName))
        spec.typeSpace = $injector.get(ns.serviceName);

      if (!(spec.typeSpace instanceof TypeSpace))
        throw providerError(name, `'${ns.serviceName}' is not a valid type service.`);
  
      /*---------------------------------------------------------*/
      /* If there is a namespace, create `TypeSpace` nodes and   */
      /* set typespace for this object factory to the last node. */
      /*---------------------------------------------------------*/
      if (ns.nameSpace)
        ns.nameSpace.split('.').forEach(function(p)
          {
          if (!spec.typeSpace[p])
            spec.typeSpace[p] = new TypeSpace();
          spec.typeSpace = spec.typeSpace[p];
          });
  
      /*--------------------------------------------------*/
      /* Create the object factory in the typespace node. */
      /*--------------------------------------------------*/
      spec.typeSpace[ns.className] = this.makeFactory(spec);
      }
    }
  }

/***************************************************************************
* TypeFactoryProvider.makeFactory */
/**
* @ngdoc    function
* @name     makeFactory
* @param    {TypeSpec} spec  Type specification.
* @return   {function} Object factory (constructor) function.
* @description
* Creates an object factory for `spec`. Called by `createType` when the
* `TypeFactory` service is instantiated.
***************************************************************************/
/* @ngInject */ function makeFactory($injector)
  {
  return function(spec)
    {
    /*-------------------------------------------------------------*/
    /* Inject `TypeFactory` as a local object to defeat "Circular  */
    /* Dependency" error.                                          */
    /* https://code.angularjs.org/1.5.11/docs/error/$injector/cdep */
    /*-------------------------------------------------------------*/
    var $svcs = { TypeFactory:this.service };

    /*----------------------------------------------*/
    /* Invoke the `compile` function to get         */
    /* constructor, prototype, and defaults values. */
    /*----------------------------------------------*/
    var compiled = $injector.invoke(spec.compile||function() { return {}; }, null, $svcs);

    if (!angular.isObject(compiled) || angular.isArray(compiled))
      throw providerError(spec.name, "'compile' function must return an object.");

    /*------------------------------------------------------------*/
    /* Get properties from specs not provide by compile function. */
    /*------------------------------------------------------------*/
    if (spec.hasOwnProperty('constructor'))
      if (!compiled.hasOwnProperty('constructor'))
        compiled.constructor = $injector.invoke(spec.constructor, null, $svcs);
      else
        throw providerError(spec.name, "'constructor' can't override 'compile' constructor.");

    if (spec.hasOwnProperty('prototype'))
      if (!compiled.hasOwnProperty('prototype'))
        if (angular.isFunction(spec.prototype))
          compiled.prototype = $injector.invoke(spec.prototype, null, $svcs);
        else
          compiled.prototype = spec.prototype;
      else
        throw providerError(spec.name, "'prototype' can't override 'compile' prototype.");

    if (spec.defaults)
      if (!compiled.defaults)
        if (angular.isFunction(spec.defaults))
          compiled.defaults = $injector.invoke(spec.defaults, null, $svcs);
        else
          compiled.defaults = spec.defaults;
      else
        throw providerError(spec.name, "'defaults' can't override 'compile' defaults.");

    /*-----------------*/
    /* Validate specs. */
    /*-----------------*/
    if (compiled.hasOwnProperty('constructor') && !angular.isFunction(compiled.constructor))
      throw providerError(spec.name, "Instance constructor must be a function.");

    if (compiled.hasOwnProperty('prototype') && (!angular.isObject(compiled.prototype) || angular.isArray(compiled.prototype)))
      throw providerError(spec.name, "Instance prototype must be an object.");

    if (!!compiled.defaults && (!angular.isObject(compiled.defaults) || angular.isArray(compiled.defaults)))
      throw providerError(spec.name, "Instance defaults must be an object.");

    /*--------------------------------------------------------------------*/
    /* Build constructor named for the class so, when viewed in           */
    /* DevTools, the object will have the class name.                     */
    /*                                                                    */
    /* https://marcosc.com/2012/03/dynamic-function-names-in-javascript/. */
    /*--------------------------------------------------------------------*/
    var ns          = new NameSpecs(spec.name);
    var actual      = (compiled.hasOwnProperty('constructor') ? compiled.constructor : defaultConstructor);
    var constructor = new Function('actual', 'return function ' +ns.className+'() { actual.apply(this, arguments); };')(actual);

    /*------------------------------------------------------------*/
    /* Add `$type` to the constructor so `TypeSpace.makeInstance` */
    /* can locate the correct class factory by `$type`.           */
    /*------------------------------------------------------------*/
    if (!!spec.$type)
      constructor.$type = spec.$type;

    /*------------------------------------------------------------*/
    /* Build class factory object prototype and defaults objects. */
    /* and give it the prototype of the inherited class, if any.  */
    /*------------------------------------------------------------*/
    var prototype = compiled.prototype||{};
    var defaults  = compiled.defaults ||{};

    /*-------------------------------------------------------*/
    /* Give the prototype a prototype of the inherited class */
    /* and combine the defaults with the parent defaults.    */
    /*-------------------------------------------------------*/
    if (spec.inherits)
      {
      var ns        = new NameSpecs(spec.inherits);
      var space     = this.types[spec.inherits].typeSpace;
      var pproto    = space[ns.className].prototype;
      var pdefaults = space[ns.className].defaults;
      prototype     = Object.combine(Object.create(pproto), prototype);
      defaults      = Object.assign({}, pdefaults, defaults);
      }

    /*-----------------------------------------------------------------------*/
    /* Add constructor to prototype as non-enumerable. This provides the     */
    /* object type name in the javascript console for the prototype.         */
    /*                                                                       */
    /* Not sure why the constructor is necessary. Testing seems to indicate  */
    /* the constructor is always the constructor and doesn't need to be set. */
    /* But `constructor` properties ($type) can't be found without this.     */
    /*-----------------------------------------------------------------------*/
    var non_enum = { constructor: { value: constructor }};

    /*-------------------------------------------------*/
    /* If a constructor was supplied, add              */
    /* `defaultConstructor` if not in prototype chain. */
    /*-------------------------------------------------*/
    if (!!compiled.constructor && !prototype.defaultConstructor)
      non_enum.defaultConstructor = { value: defaultConstructor };

    /*-----------------------------------------------------------------------------------------------------------------*/
    /* If `toJSON` is not in the prototype chain, assign `defaultToJSON` to add `$type`.                               */
    /* Ignore any `toJSON()` parameter as `defaultToJSON` must be called without one to return `this`.                 */
    /* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify#tojson_behavior */
    /*-----------------------------------------------------------------------------------------------------------------*/
    if (!prototype.toJSON)
      non_enum.toJSON             = { value: function() { return defaultToJSON.call(this); }};

    /*--------------------------------------------*/
    /* If `toJSON` was supplied, add              */
    /* `defaultToJSON` if not in prototype chain. */
    /*--------------------------------------------*/
    else if (!prototype.defaultToJSON)
      non_enum.defaultToJSON      = { value: defaultToJSON };

    /*-----------------------------------*/
    /* Add non-enumerable properties to  */
    /* prototype and return constructor. */
    /*-----------------------------------*/
    Object.defineProperties(prototype, non_enum);

    constructor.defaults    = defaults;
    constructor.prototype   = prototype;
    constructor.newInstance = newInstance;
    constructor.typeFactory = this;

    return constructor;
    };
  
  /*****************************************************************************
  * prototype.defaultConstructor
  *
  * Constructor assigned to the type if none provided. Returns a new object with 
  * `prototype`, default values, and shallow copy of `data` properties. If a
  * constructor is provided, it should call this method to assign `defaults`. 
  * 
  * @param  {object} data  Properties to copy to new type instance.
  * @return {object}       New type instance object with `data` properties.
  *****************************************************************************/
  function defaultConstructor(data)
    {
    return Object.assign(this, this.constructor.defaults, data||{});
    }
  
  /*****************************************************************************
  * prototype.defaultToJSON
  *
  * `toJSON` method assigned to the type if none provided. Returns a shallow
  * copy of `data` or `this` with `$type` added. if a `toJSON` is provided in
  * the prototype, it should call this method to add `$type`.
  * 
  * @param  {object} data  Properties to return. `null` == use `this`.
  * @return {object}       Properties to serialize.
  *****************************************************************************/
  function defaultToJSON(data)
    {
    var rv = Object.assign({}, data||this);
    if (!!this.constructor.$type)
      rv.$type = this.constructor.$type;
    return rv;
    }

  /*****************************************************************************
  * constructor.newInstance
  *
  * Calls `TypeFactory.makeInstance` to create an instance of a type for the
  * `data.$type` or from this type constructor if `$type` is not found. This 
  * method should be called when `$type` may be a subclass of this type.
  * 
  * @param  {object} data  Properties to copy to new type instance.
  * @return {object}       New `$type` instance object with `data` properties.
  *****************************************************************************/
  function newInstance(obj)
    {
    return this.typeFactory.makeInstance(obj, this);
    }
  }

/***************************************************************************
* TypeFactoryProvider.makeInstance */
/**
* @ngdoc    function
* @name     makeInstance
* @param    {object}   obj                Properties to assign.
* @param    {function} defaultConstructor Default constructor.
* @return   {object}   Instantiated object.
* @description
* Instantiates an object based on `obj.$type`. Called by object factory
* `newInstance()` method. `TypeSpace.makeInstance()` is called in each of
* the services in `spaces` until an object is returned. `defaultConstructor`
* is used to instantiate the object if not found in the `TypeSpace` tree
* or `obj.$type` is not provided.
***************************************************************************/
/* @ngInject */ function makeInstance()
  {
  return function(obj, defaultConstructor)
    {
    if (!!obj && obj.hasOwnProperty('$type'))
      {
      var instance;
      for (var i=0; i < this.spaces.length; i++)
        if (!!(instance = this.spaces[i].makeInstance(obj, true)))
          return instance;
      }

    return new defaultConstructor(obj);
    };
  }

/*******************************************************************************
* NameSpecs */
/**
* @ngdoc    object
* @name     NameSpace
* @constructor
* @param {string} name  Type `name` as `<serviceName>[.<namespace>].<className>`.
* @description
* Returns an object with `name` parts:
* 
*   * `className`   - Client-side class name.
*   * `nameSpace`   - Type namespace. Empty string if none.
*   * `serviceName` - Service factory name.
*   * `typeSpace`   - `<serviceName>[.<namespace>]` name of `TypeSpace` node.
*******************************************************************************/
function NameSpecs(name)
  {
  var parts = (name||'').split('.');

  if (parts.length < 2)
    throw providerError(null, "'name' must be '<serviceName>[.<namespace>].<className>'.");

  this.className   = parts.pop();
  this.typeSpace   = parts.join('.');
  this.serviceName = parts.shift();
  this.nameSpace   = parts.join('.');
  }

/*******************************************************************************
* providerError */
/**
* Returns a provider error message.
*******************************************************************************/
function providerError(name, message)
  {
  return `TypeFactoryProvider: ${!!name ? name+': ' : ''}${message}`;
  }

/*******************************************************************************
* TypeSpace */
/**
* @ngdoc    object
* @name     TypeSpace
* @constructor
* @description
* Node in `<serviceName>[.<namespace>]` tree where object factories are created.
* Includes `makeInstance()` method for instantiating an object by `$type`.
*******************************************************************************/
function TypeSpace() {}

TypeSpace.prototype =
  {
  /*****************************************************************************
  * makeInstance */
  /**
  * @ngdoc    function
  * @name     makeInstance
  * @methodOf TypeSpace
  * @param    {object}  data   Server serialized object with `$type` property.
  * @param    {boolean} quiet  `true`=suppress exceptions on recursive call.
  * @return   {object}  Instantiated object for `data.$type` or `null`.
  *
  * @description
  * Traverses the `TypeSpace` tree to find the object factory (constructor)
  * for `data.$type` and returns an instance with `data` properties. `$type` is
  * removed from the new instance since it will be added on serialization.
  *
  * This method is called recursively for each `TypeSpace` node in the tree and
  * throws an exception if `$type` is not part of `data` or cannot be found on
  * any of the `TypeSpace` nodes.
  *****************************************************************************/
  makeInstance: function(data, quiet)
    {
    if (!quiet && !data.$type)
      throw "TypeSpace.makeInstance: '$type' property not found on object.";

    /*---------------------------------------*/
    /* Find the object factory for           */
    /* `data.$type` and create the instance. */
    /*---------------------------------------*/
    for (var p in this)
      {
      var pobject  = this[p];
      var instance = null;

      /*--------------------------------------*/
      /* If this object is the matching       */
      /* object factory, create the instance. */
      /*--------------------------------------*/
      if (angular.isFunction(pobject) && pobject.$type === data.$type)
        instance = new pobject(data);

      /*----------------------------------------*/
      /* If this object is another TypeSpace, */
      /* check its object factories.            */
      /*----------------------------------------*/
      else if (pobject instanceof TypeSpace)
        instance = pobject.makeInstance(data, true);

      /*------------------------------------------------------------*/
      /* If the instance was created, remove `$type` and return it. */
      /*------------------------------------------------------------*/
      if (instance)
        {
        delete instance.$type;
        return instance;
        }
      }
        
    if (!quiet)          
      throw "TypeSpace.makeInstance: factory not found for object `$type` '"+data.$type+"'";
    }
  };
})();
