# TypeFactory
  
Provides configuration of type "classes" that correspond to server C# classes.

<!-- toc -->

- [Overview](#overview)
  * [Creating Classes in `.config()` Blocks](#creating-classes-in-config-blocks)
  * [De/Serialization of Server-Side Objects](#deserialization-of-server-side-objects)
  * [The Typespace Tree](#the-typespace-tree)
- [Type Specifications](#type-specifications)
- [Object Factories (Constructors)](#object-factories-constructors)
      - [Default Constructor](#default-constructor)
  * [Prototype](#prototype)
- [Services](#services)
  * [mapObject](#mapobject)
  * [mapList](#maplist)

<!-- tocstop -->

## Overview

This tool was created to address two issues: 

  1. Creation of namespace organizational structure of client-side object
     factories in `.config()` rather than `.run()` blocks, and
     
  2. De/Serialization of server-side objects to/from client-side class and
     subclass objects using a serialized `$type` property.


### Creating Classes in `.config()` Blocks

For each client-side class, a **Type Specification** is given to the provider 
`addType()` method in a `.config()` block. The `name` property defines the 
class name and location within the [Typespace Tree](#the-typespace-tree). For example, 
"Data.people.Parent" has a class name of "Parent" and a typespace of "Data.people". 
A class object can be instantiated by calling `new Data.people.Parent(obj)` or 
`Data.people.Parent.newInstance(obj)`.


### De/Serialization of Server-Side Objects

A great challenge for synchronization between C# and Javascript objects is 
correctly selecting the object constructor (factory) to use when instantiating
a class or subclass. The only way to do so is to include the object "Type" as
a property in the object serialization. This way, both the server and the client
can use this property value to instantiate using the appropriate class.

**JSON.Net** provides serialization settings to include `$type` as the class name
in the serialization of server-side objects and deserialization into server-side
class objects by the value of the `$type` property from the client.

The object factories created by `TypeFactory` have a [`newInstance()`](#constructor.newInstance) method 
that instantiates the object using the appropriate constructor based on the 
`$type` property provided by the server. They also provide a `toJSON()` 
method that will add the `$type` property to the serialized object for use in 
server deserialization.


### The Typespace Tree

This tool creates a tree of `TypeSpace` nodes, one for each segment of a 
fully-qualified class name. Each node property is another `TypeSpace` node or 
an [object factory (constructor)](#object-factories-constructors).

In this `TypeSpace` tree,
```
                          -- Parent
                         /
             -- people -- 
            /            \
           /              -- Teacher
    Data --              
           \              -- School 
            \            /
             -- places -- 
                         \
                          -- Home
```
- `Data` is an AngularJS Factory and a `TypeSpace` node created in `addType()`.
- `Data.people` and `Data.places` are `TypeSpace` nodes.
- `Data.people.Parent`, `Data.people.Teacher`, `Data.places.School` and 
  `Data.places.Home` are class constructors.

Service factories, like "Data", are created in the `addType()` method. All other 
`TypeSpace` nodes and all object factories are created when this service is instantiated.


## Type Specifications

Type specification object passed to `addType()` has properties:

  - <a name="spec.name">`{string}`**`name`**</a>  
    Client side type name as `<serviceName>[.<namespace>].<className>`.
    
    - `serviceName`  
      Service factory name. For example, `Data.books.AccountInfo` would be
      created in the service `Data`.
    
    - `namespace`  
      Namespace within the service factory. Should be a logical separation for
      types within a common environment. For instance, types used only in
      an application would have no namespace and be qualified only as 
      `Data.ClassName`. Books Application types should be qualified as
      `Data.books.ClassName`. Ideally, `namespace` would match the namespace
      of the server side class.
      
    - `className`  
      Client-side class name. This should usually match the server class name.
      
  - <a name="spec.type">`{string}`**`$type`**</a>  
    Optional fully qualified name of the corresponding class on the server 
    formatted as `<namespace>.<className>, <assemblyName`. This property 
    will be added to the JSON serialized form of the object so **JSON.Net** 
    can deserialize into the correct class. It is also used to identify the
    correct type to create for a server serialized object instantiated on
    the client with `<serviceName>.<nameSpace>[.<className>].makeInstance()`.
    
  - <a name="spec.inherits">`{string}`**`inherits`**</a>  
    Optional client-side name of parent type. Object of parent type becomes
    prototype of `prototype` object.

  - <a name="spec.prototype">`{function|object}`**`prototype`**</a>  
    Optional prototype object for type instantiated objects. May be a
    function returning an object from `$injector.invoke()`.
    
  - <a name="spec.defaults">`{function|object}`**`defaults`**</a>  
    Optional object of default values for type instantiated objects. May be a
    function returning an object from `$injector.invoke()`. Any property that 
    must be serialized to a value should be included here.
    
    Note: The [Default Constructor](#default-constructor) will assign these
    values to new instances of the type. If a custom constructor is provided,
    it must either call the default constructor or assign these values from this
    object stored in the instance `constructor.defaults` property. 
    
  - <a name="spec.constructor">`{function}`**`constructor`**</a>  
    Optional function returning a function taking an optional, deserialized 
    server object. This function must either call the [default constructor](#default-constructor) 
    or assign the object parameter properties to the instance.
    
  - <a name="spec.compile">`{function}`**`compile`**</a>  
    Optional function returning an object from `$injector.invoke()` as an
    alternative to the specification properties. The object returned from
    may include:
      - `{function}`**`constructor`** - Type constructor function.
      - `{object}`**`prototype`**     - New instance prototype object.
      - `{object}`**`defaults`**      - New instance default values.
    
Properties Assigned Internally and Not Included in Specification

  - `{TypeSpace}`**`typeSpace`**  
    `TypeSpace` service where the type object factory is created.  


## Object Factories (Constructors)

Created from Type Specifications. Objects created with the factory include a 
`toJSON()` method that adds `spec.$type` to the object to be serialized by 
`JSON.stringify()`. Thus any object specification that includes `$type`, 
identifying a server-side class, can be deserialized on the server with 
`JSON.Net` using this `$type` value.

Constructor properties:

 - `{string}`**`$type`**  
   Copied from `spec.$type` for object instantiation using `newInstance()`.

 - <a name="constructor.newInstance">`{function}`**`newInstance`**  
   Creates an instance of the type or a subtype. Called with an object returned 
   from the server with a `$type` property. The subtype with the matching `$type` 
   value is used to instantiate the object and assign the properties from the 
   server object. If the matching subtype is not found, an instance of this type 
   is instantiated..

 - `{object}`**`prototype`**  
   New instance prototype object.
   
 - `{object}`**`defaults`**  
   Default values to assign to each new instance.

 - `{object}`**`typeFactory`**  
   Reference to `TypeFactoryProvider` for access to internal methods.
   

##### Default Constructor

The default constructor takes an optional object parameter. It instantiates the 
new type instance, assigns the [defaults](#spec.defaults), if any, and assigns 
any parameter properties.

If a custom constructor is provided in the [`constructor`](#spec.constructor)
property or from the [`compile`](#spec.compile) function, it must either call
this method or assign the [`defaults`](#spec.defaults) and parameter properties.


### Prototype

The [prototype](#spec.prototype) object of new instances is created from the 
Type specifications. If the type has a parent type, the prototype of the new 
instance prototype is an instance of the parent type.

In addition to any properties defined, this object includes:

Object properties:

  - `{function}`**`constructor`**  
    Constructor function. Required for access to constructor properties.

  - `{function}`**`defaultConstructor([obj])`**  
    Default constructor function. Assigned if `spec.constructor` is not
    empty so the user supplied constructor may call the default. Instantiates
    the object, assigns [defaults](#defaults), and copies properties from
    **`obj`**, if supplied.

  - `{function}`**`toJSON`**  
    Assigned if `spec.prototype` object does not include `toJSON` method.
    Adds `spec.$type` to the object to be serialized by `JSON.stringify()`.

  - `{function}`**`defaultToJSON([obj])`**  
    Assigned if `spec.prototype` object includes `toJSON` method so it may
    be called by `toJSON`. Returns a new object with properties from **`obj`**,
    or `this` if `obj` not supplied, and `$type` set to [`spec.$type`](#spec.type).


## Services

### mapObject

  - ***parameters***
    - `{constructor}` [Object constructor](#object-factories-constructors).
  - ***returns***
    - `{function}` Function to convert anonymous object.  
      
Returns a function that takes input of an anonymous object and returns an instance of the constructor
class from a call to the constructor [`newInstance()`](#constructor.newinstance) method.

```js
var productInfo = TypeFactory.mapObject(Data.books.Product)({name:"Book", ...});
```
   
### mapList

  - ***parameters***
    - `{constructor}` [Object constructor](#object-factories-constructors).
  - ***returns***
    - `{function}` Function to convert array of anonymous objects.  
      
Returns a function that calls `mapObject(constructor)` for each element of an array of 
anonymous objects and returns the converted array.

```js
productInfo.subItems = TypeFactory.mapList(Data.books.Product)(productInfo.subItems);
```
