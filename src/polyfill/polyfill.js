/*******************************************************************************
* $Id: polyfill.js 20541 2020-11-18 02:54:31Z sthames $
/**
* @ngdoc    overview
* @name     csm.polyfill
* @description
* Modifies the native Javascript environment to add Polyfills. 
*******************************************************************************/
'use strict'; angular.module('wsajs.polyfill', []);
