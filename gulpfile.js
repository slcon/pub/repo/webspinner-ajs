/*****************************************************************************************
* $Id: gulpfile.js 20566 2020-12-01 00:03:44Z sthames $
* ----------------------------------------------------------------------------------------
* Gulp configuration for WebSpinnerAJS library.
*****************************************************************************************/
const gulp     = require('gulp');
const colors   = require('ansi-colors');
const debug    = require('gulp-debug');
const fs       = require('fs');
const log      = require('fancy-log');
const Q        = require('q');
const common   = require('@slcpub/gulp-common').useGulp(gulp);
const npm      = require('@slcpub/gulp-npm').useGulp(gulp);
const buildLib = require('@csmcentral/csm-build-tools').lib({ libName: 'webspinner-ajs', libPrefix: 'wsajs' });

common.addTasks();
npm   .addTasks();

gulp.task('default', () => common.showUsage());

/*****************************************************************************************
* Tasks
*****************************************************************************************/
/**
* Builds the library into the `dist` folder. Removes `bower.json` that was copied to `dist`.
* 
* @task  {build-lib}
* @group {Supporting Tasks}
* @order {200}
*/
gulp.task('build-lib', () => buildLib.build().on('end', () => Q.resolve(fs.unlinkSync("./dist/bower.json", {force:true}))));

/**
* Removes distribution and minified file test folders.
* 
* @task  {clean-build}
* @group {Supporting Tasks}
* @order {200}
*/
gulp.task('clean-build', () => buildLib.clean());

/**
* Drops `css` and `css.map` files for all `scss` files.
* 
* @task  {clean-sass}
* @group {Supporting Tasks}
* @order {200}
*/
gulp.task('clean-sass', () => buildLib.cleanSASS());

/**
* Builds `css` and `css.map` files for all `scss` files.
* 
* @task  {compile-sass}
* @group {Supporting Tasks}
* @order {200}
*/
gulp.task('compile-sass', ['clean-sass'], () => buildLib.compileSASS());

/**
* Update table of contents in all Markdown files.
* 
* @task  {update-all-toc}
* @group {Supporting Tasks}
* @order {200}
*/
gulp.task('update-all-toc', () => 
  common.updateTOC([ '*.md', 'src/**/*.md' ])
    .progress(file => log(`Updated ${colors.yellow(file.relative)}`)));

/**
* Builds the library.
* 
* @task  {build}
* @group {Build Tasks}
* @order {100}
*/
gulp.task('build', () => common.runTasks('clean', 'update-bower.json', 'compile-sass', 'build-lib'));

/**
* Removes distribution and minified file test folders.
* 
* @task  {clean}
* @group {Build Tasks}
* @order {100}
*/
gulp.task('clean', () => common.runTasks('clean-build', 'clean-sass'));
